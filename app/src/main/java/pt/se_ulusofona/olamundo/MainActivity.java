package pt.se_ulusofona.olamundo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView msg = (TextView)
                findViewById(R.id.text);

        msg.setText("Ola Mundo");

        msg.setTextSize(19);

        showNumbers();

        showNames();


    }
    
    private void showNumbers() {
        TextView numbers = (TextView)
                findViewById(R.id.numbers);
        numbers.setText("");
        for (int i = 0; i <= 50 ; i++) {
            if (i % 2 == 0) {
                numbers.append(Integer.toString(i) + " ");
            }
        }
    }

    private void showNames() {
        TextView names = (TextView)
                findViewById(R.id.names);
        names.setText("");

        List<String> namesList = Arrays.asList("Mário", "Carlos", "Jorge", "Luis", "Rui", "Ana", "Joana", "Zé", "Francisco", "António");
        for (String name:namesList) {
            names.append(name + " - "+ name.length() +"\n");
        }
    }
}
